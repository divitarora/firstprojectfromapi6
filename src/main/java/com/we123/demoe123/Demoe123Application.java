package com.we123.demoe123;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demoe123Application {

	public static void main(String[] args) {
		SpringApplication.run(Demoe123Application.class, args);
	}

}
